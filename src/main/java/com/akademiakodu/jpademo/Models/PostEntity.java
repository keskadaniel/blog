package com.akademiakodu.jpademo.Models;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name="post")
@Data
public class PostEntity {

        @Id
        @GeneratedValue
        private int id;
        private String title;
        private String content;
        private int user_id;
        private LocalDateTime data;

}
