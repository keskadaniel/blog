package com.akademiakodu.jpademo.Models.repositories;

import com.akademiakodu.jpademo.Models.PostEntity;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<PostEntity, Integer> {

}
