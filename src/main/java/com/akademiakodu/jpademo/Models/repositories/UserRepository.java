package com.akademiakodu.jpademo.Models.repositories;
import com.akademiakodu.jpademo.Models.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

    boolean existsByUsername(String username);
    boolean existsByUsernameAndPassword(String username, String password);

    @Override
    Iterable<UserEntity> findAllById(Iterable<Integer> iterable);

    Optional<UserEntity> findByUsernameAndPassword(String username, String password);
}
