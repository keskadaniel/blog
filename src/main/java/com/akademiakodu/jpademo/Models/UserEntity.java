package com.akademiakodu.jpademo.Models;

import com.akademiakodu.jpademo.Models.repositories.PostRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="user")
@Data
public class UserEntity {
    @Id
    @GeneratedValue
    private int id;
    private String username;
    private String password;
    private String email;


}
