package com.akademiakodu.jpademo.Models.Services;

import com.akademiakodu.jpademo.Models.UserEntity;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, scopeName = "session")
@Data
public class UserService {
    private boolean isLogin;
    private UserEntity user;
}
