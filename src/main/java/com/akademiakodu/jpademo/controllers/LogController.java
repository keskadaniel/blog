package com.akademiakodu.jpademo.controllers;

import com.akademiakodu.jpademo.Models.Services.UserService;
import com.akademiakodu.jpademo.Models.UserEntity;
import com.akademiakodu.jpademo.Models.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.swing.text.html.Option;
import java.util.Optional;

@Controller
public class LogController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @GetMapping("/login")
    public String login() {
        return "login";
    }


    @PostMapping("/login")
    public String login (@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           Model model
    ) {

        Optional<UserEntity> user = userRepository.findByUsernameAndPassword(username, password);

        if(user.isPresent()) {
            userService.setLogin(true);
            userService.setUser(user.get());
            return "redirect:/";
        }

        model.addAttribute("info", "zarejestruj się");
        return "redirect:/register";
    }

    //aby nie dało sie zalogować na 2 kompach

}
