package com.akademiakodu.jpademo.controllers;

import com.akademiakodu.jpademo.Models.PostEntity;
import com.akademiakodu.jpademo.Models.Services.UserService;
import com.akademiakodu.jpademo.Models.UserEntity;
import com.akademiakodu.jpademo.Models.repositories.PostRepository;
import com.akademiakodu.jpademo.Models.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PostRepository postRepository;

    @Autowired
    UserService userService;


//    @GetMapping("/")
//    public String index(Model model){
//        model.addAttribute("user", userService);
//        return "index";
//    }


    @GetMapping("/register")
    public String register(){
        return "register";
    }

    @PostMapping("/register")
    public String register(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("repeatPassword") String repeatPassword,
                           @RequestParam("email") String email,
                           Model model
    ){
        if (!password.equals(repeatPassword)) {
            model.addAttribute("info", "hasło się nie zgadza");
            return "register";
        }

        if (userRepository.existsByUsername(username)) {
            model.addAttribute("info", "nick zajety");
            return "register";
        }


        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(username);
        userEntity.setPassword(password);
        userEntity.setEmail(email);

        userRepository.save(userEntity);
        model.addAttribute("info", "zarejestrowano");
        return "register";
    }

}
