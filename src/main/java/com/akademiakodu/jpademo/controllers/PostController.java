package com.akademiakodu.jpademo.controllers;

import com.akademiakodu.jpademo.Models.PostEntity;
import com.akademiakodu.jpademo.Models.Services.UserService;
import com.akademiakodu.jpademo.Models.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class PostController {

    @Autowired
    PostRepository postRepository;

    @Autowired
    UserService userService;

    @GetMapping("/post")
    public String post(){
        return "post";
    }

    @PostMapping("/post")
    @ResponseBody
    public String post(@RequestParam("title") String title,
                       @RequestParam("content") String content){

        UserService user = userService;

            PostEntity postEntity = new PostEntity();
            postEntity.setTitle(title);
            postEntity.setContent(content);
            postEntity.setUser_id(user.getUser().getId());

            postRepository.save(postEntity);


        return "3 post";
    }

    //nowe rzeczy
    @RequestMapping(value="/", method= RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("posts", postRepository.findAll());

        return "index";
    }
  //  https://springframework.guru/using-jdbctemplate-with-spring-boot-and-thymeleaf/

}
